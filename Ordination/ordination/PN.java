package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	ArrayList<LocalDate> husk = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, double antalEnheder) {
		super(startDen, slutDen, patient);
		this.antalEnheder = antalEnheder;
	}

	private double antalEnheder;
	private int counter = 0;

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean givet = false;
		if (givesDen != null) {
			if (super.getStartDen().isBefore(getSlutDen()) && givesDen.isBefore(getSlutDen())
					&& givesDen.isAfter(getStartDen()) || givesDen.equals(getSlutDen())
					|| givesDen.equals(getStartDen())) {

				husk.add(givesDen);
				counter++;
				givet = true;
			}
		}
		return givet;
	}

	@Override
	public double doegnDosis() {
		int antaldageForSidsteGivning = 0;
		double antal = 0;
		try {
			antaldageForSidsteGivning = (int) ChronoUnit.DAYS.between(husk.get(0), husk.get(this.husk.size() - 1)) + 1;
			antal = samletDosis() / antaldageForSidsteGivning;

		} catch (IndexOutOfBoundsException e) {
			e.getMessage();
		}
		return antal;
	}

	@Override
	public double samletDosis() {

		return getAntalEnheder() * getAntalGangeGivet();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return counter;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN Ordination";
	}

	public ArrayList<LocalDate> getHusk() {
		return new ArrayList<>(husk);
	}

	public void addAndvendelseAfPNOrdination(LocalDate date) {
		husk.add(date);
	}

}
