package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {
	PN pn;
	Patient p1;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSamletDosis() {
		// Test 1
		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 03), p1, 4);
		pn.givDosis(LocalDate.of(2017, 02, 01));
		pn.givDosis(LocalDate.of(2017, 02, 01));
		pn.givDosis(LocalDate.of(2017, 02, 01));

		pn.givDosis(LocalDate.of(2017, 02, 02));
		pn.givDosis(LocalDate.of(2017, 02, 02));
		pn.givDosis(LocalDate.of(2017, 02, 02));

		pn.givDosis(LocalDate.of(2017, 02, 03));
		pn.givDosis(LocalDate.of(2017, 02, 03));
		pn.givDosis(LocalDate.of(2017, 02, 03));

		assertEquals(36.0, pn.samletDosis(), 0.01);

		// test 2

		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 04), LocalDate.of(2017, 02, 05), p1, 20);
		pn.givDosis(LocalDate.of(2017, 02, 04));
		pn.givDosis(LocalDate.of(2017, 02, 04));
		pn.givDosis(LocalDate.of(2017, 02, 04));

		pn.givDosis(LocalDate.of(2017, 02, 05));
		pn.givDosis(LocalDate.of(2017, 02, 05));
		pn.givDosis(LocalDate.of(2017, 02, 05));

		assertEquals(120, pn.samletDosis(), 0.1);
	}

	@Test
	public void testDoegnDosis() {
		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 8), p1, 5);
		pn.givDosis(LocalDate.of(2017, 02, 01));
		pn.givDosis(LocalDate.of(2017, 02, 03));
		pn.givDosis(LocalDate.of(2017, 02, 04));
		pn.givDosis(LocalDate.of(2017, 02, 8));

		assertEquals(2.5, pn.doegnDosis(), 0.1);

		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 5), p1, 20);
		pn.givDosis(LocalDate.of(2017, 02, 01));
		pn.givDosis(LocalDate.of(2017, 02, 03));
		pn.givDosis(LocalDate.of(2017, 02, 04));
		pn.givDosis(LocalDate.of(2017, 02, 5));

		assertEquals(16, pn.doegnDosis(), 0.1);

		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 8), p1, 100);
		pn.givDosis(LocalDate.of(2017, 02, 01));
		pn.givDosis(LocalDate.of(2017, 02, 03));
		pn.givDosis(LocalDate.of(2017, 02, 04));
		pn.givDosis(LocalDate.of(2017, 02, 8));

		assertEquals(50, pn.doegnDosis(), 0.1);

	}

	@Test
	public void testGetType() {
		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 8), p1, 100);

		assertEquals("PN Ordination", pn.getType());

	}

	@Test
	public void testGetAntalEnheder() {
		p1 = new Patient("200000-0000", "Benjamin", 20);
		pn = new PN(LocalDate.of(2017, 02, 01), LocalDate.of(2017, 02, 8), p1, 100);

		assertEquals(100, pn.getAntalEnheder(), 0.1);

	}

}
