package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosis;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient) {
		super(startDen, slutDen, patient);
		this.dosis = new ArrayList<>();

	}

	public Dosis opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		this.dosis.add(dosis);
		return dosis;
	}

	public ArrayList<Dosis> getDosis() {
		return new ArrayList<>(dosis);
	}

	public void removeDosis(Dosis dosis) {
		this.dosis.remove(dosis);
	}

	@Override
	public double samletDosis() {
		return super.antalDage() * doegnDosis();

	}

	@Override
	public double doegnDosis() {
		double samlet = 0;
		for (Dosis d : dosis) {
			samlet += d.getAntal();
		}
		return samlet;
	}

	@Override
	public String getType() {
		return "Daglig Skæv Ordination";
	}
}
