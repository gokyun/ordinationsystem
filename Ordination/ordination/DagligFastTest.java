package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {

	DagligFast df1, df2;
	Patient p1;
	Dosis d1, d2, d3, d4;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSamletDosis() {
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 03), p1);
		df1.createDosis(LocalTime.of(06, 00), 1);
		df1.createDosis(LocalTime.of(12, 00), 1);
		df1.createDosis(LocalTime.of(18, 00), 1);
		df1.createDosis(LocalTime.of(00, 00), 1);

		Assert.assertEquals(12, df1.samletDosis(), 0.1);

		p1 = new Patient("1200000", "Benjamin", 100);
		df2 = new DagligFast(LocalDate.of(2017, 01, 03), LocalDate.of(2017, 01, 03), p1);
		df2.createDosis(LocalTime.of(06, 00), 4);
		df2.createDosis(LocalTime.of(12, 00), 4);
		df2.createDosis(LocalTime.of(18, 00), 4);
		df2.createDosis(LocalTime.of(00, 00), 0);

		Assert.assertEquals(12.0, df2.samletDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis() {

		// Test 1
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);
		df1.createDosis(LocalTime.of(06, 00), 120);
		df1.createDosis(LocalTime.of(12, 00), 120);
		df1.createDosis(LocalTime.of(18, 00), 120);
		df1.createDosis(LocalTime.of(00, 00), 120);

		Assert.assertEquals(480, df1.doegnDosis(), 0.1000);

		// Test 2
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);
		df1.createDosis(LocalTime.of(06, 00), 0);
		df1.createDosis(LocalTime.of(12, 00), 40);
		df1.createDosis(LocalTime.of(18, 00), 40);
		df1.createDosis(LocalTime.of(00, 00), 40);

		Assert.assertEquals(120, df1.doegnDosis(), 0.1000);

		// Test 3
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);
		df1.createDosis(LocalTime.of(06, 00), 0);
		df1.createDosis(LocalTime.of(12, 00), 20);
		df1.createDosis(LocalTime.of(18, 00), 20);
		df1.createDosis(LocalTime.of(00, 00), 20);

		Assert.assertEquals(60, df1.doegnDosis(), 0.1000);

		// Test 4
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);
		df1.createDosis(LocalTime.of(06, 00), 0);
		df1.createDosis(LocalTime.of(12, 00), 0);
		df1.createDosis(LocalTime.of(18, 00), 10);
		df1.createDosis(LocalTime.of(00, 00), 10);

		Assert.assertEquals(20, df1.doegnDosis(), 0.1000);

		// Test 5
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);
		df1.createDosis(LocalTime.of(06, 00), 0);
		df1.createDosis(LocalTime.of(12, 00), 0);
		df1.createDosis(LocalTime.of(18, 00), 0);
		df1.createDosis(LocalTime.of(00, 00), 5);
		Assert.assertEquals(5, df1.doegnDosis(), 0.100);

	}

	@Test
	public void testGetType() {
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.now(), p1);

		Assert.assertEquals("Daglig Fast Ordination", df1.getType());

	}

	@Test
	public void testCreateDosis() {
		p1 = new Patient("1200000", "Benjamin", 100);
		df1 = new DagligFast(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 01), p1);
		d1 = new Dosis(LocalTime.of(12, 00), 10);

		d1 = df1.createDosis(LocalTime.of(12, 00), 10);

		assertEquals(d1, df1.getDosis()[1]);

		d1 = df1.createDosis(LocalTime.of(18, 00), 40);

		assertEquals(d1, df1.getDosis()[2]);
	}

}
