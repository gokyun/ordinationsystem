package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	DagligSkaev ds1;
	Patient p1;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSamletDosis() {
		// Test 1
		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 01), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 10);
		ds1.opretDosis(LocalTime.of(12, 00), 10);
		ds1.opretDosis(LocalTime.of(18, 00), 10);
		ds1.opretDosis(LocalTime.of(00, 00), 10);

		assertEquals(40, ds1.samletDosis(), 0.1000);

		// Test 2
		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 01), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 20);
		ds1.opretDosis(LocalTime.of(12, 00), 20);
		ds1.opretDosis(LocalTime.of(18, 00), 20);
		ds1.opretDosis(LocalTime.of(00, 00), 20);

		assertEquals(80, ds1.samletDosis(), 0.1000);

		// Test 3
		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 01), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 30);
		ds1.opretDosis(LocalTime.of(12, 00), 30);
		ds1.opretDosis(LocalTime.of(18, 00), 30);
		ds1.opretDosis(LocalTime.of(00, 00), 30);

		assertEquals(120, ds1.samletDosis(), 0.1000);

		// Test 4
		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 01), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 40);
		ds1.opretDosis(LocalTime.of(12, 00), 40);
		ds1.opretDosis(LocalTime.of(18, 00), 40);
		ds1.opretDosis(LocalTime.of(00, 00), 40);

		Assert.assertEquals(160, ds1.samletDosis(), 0.1000);

	}

	@Test
	public void testDoegnDosis() {

		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 02), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 4);
		ds1.opretDosis(LocalTime.of(12, 00), 4);
		ds1.opretDosis(LocalTime.of(18, 00), 4);
		ds1.opretDosis(LocalTime.of(00, 00), 4);
		ds1.opretDosis(LocalTime.of(00, 00), 4);

		assertEquals(20, ds1.doegnDosis(), 0.1000);

		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 04), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 10);
		ds1.opretDosis(LocalTime.of(12, 00), 10);
		ds1.opretDosis(LocalTime.of(18, 00), 10);
		ds1.opretDosis(LocalTime.of(00, 00), 10);

		assertEquals(40, ds1.doegnDosis(), 0.1000);

		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 05), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 20);
		ds1.opretDosis(LocalTime.of(12, 00), 20);
		ds1.opretDosis(LocalTime.of(18, 00), 20);

		assertEquals(60, ds1.doegnDosis(), 0.1000);

		p1 = new Patient("1200000", "Frank", 100);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 07), p1);
		ds1.opretDosis(LocalTime.of(06, 00), 50);
		ds1.opretDosis(LocalTime.of(12, 00), 50);

		assertEquals(100, ds1.doegnDosis(), 0.1000);

	}

	@Test
	public void testGetType() {
		p1 = new Patient("121232-1232", "Dennis", 85);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 04), LocalDate.of(2017, 03, 05), p1);

		assertEquals("Daglig Skæv Ordination", ds1.getType());

	}

	@Test
	public void testOpretDosis() {
		p1 = new Patient("121232-1232", "Dennis", 85);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 04), LocalDate.of(2017, 03, 05), p1);

		Dosis d1 = ds1.opretDosis(LocalTime.of(8, 00), 4);
		assertEquals(d1, ds1.getDosis().get(0));

	}

	@Test
	public void testGetDosis() {
		p1 = new Patient("121232-1232", "Dennis", 85);
		ds1 = new DagligSkaev(LocalDate.of(2017, 03, 04), LocalDate.of(2017, 03, 05), p1);

		Dosis d1 = ds1.opretDosis(LocalTime.of(8, 00), 50);

		assertEquals(d1, ds1.getDosis().get(0));
	}

}
