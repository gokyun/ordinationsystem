package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis dosis[];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient) {
		super(startDen, slutDen, patient);
		this.dosis = new Dosis[4];
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		addDosis(dosis);
		return dosis;
	}

	private void addDosis(Dosis dosis) {
		if (dosis.getTid().equals(LocalTime.of(06, 00))) {
			this.dosis[0] = dosis;
		} else if (dosis.getTid().equals(LocalTime.of(12, 00))) {
			this.dosis[1] = dosis;
		} else if (dosis.getTid().equals(LocalTime.of(18, 00))) {
			this.dosis[2] = dosis;
		} else {
			this.dosis[3] = dosis;
		}
	}

	public void removeDosis(int where) {
		this.dosis[where] = null;
	}

	public Dosis[] getDosis() {
		return dosis;
	}

	@Override
	public double samletDosis() {
		return super.antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		double samlet = 0;
		for (Dosis d : dosis) {
			samlet += d.getAntal();
		}
		return samlet;
	}

	@Override
	public String getType() {

		return "Daglig Fast Ordination";
	}

}
