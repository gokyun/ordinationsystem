package service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest {

	Service service;
	Patient p1;
	Laegemiddel lm;
	PN pn, pn2;
	DagligFast df;
	DagligSkaev ds;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		p1 = new Patient("200000-0000", "Leo", 5);
		lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

	}

	@Test
	public void testOpretPNOrdination() {

		pn2 = service.opretPNOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 02), p1, lm, 50);

		assertNotNull(pn2);

		pn2 = service.opretPNOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 07, 01), p1, lm, 50);

		assertNotNull(pn2);

		pn2 = service.opretPNOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2018, 04, 03), p1, lm, 50);

		assertNotNull(pn2);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdinationFail() {
		pn = service.opretPNOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 02, 28), p1, lm, 50);
		pn2 = service.opretPNOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2018, 04, 03), p1, lm, 50);

		assertEquals(pn, pn2);

	}

	@Test
	public void testOpretDagligFastOrdination() {
		df = service.opretDagligFastOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 03, 03), p1, lm, 50, 50,
				50, 50);
		assertNotNull(df);

		df = service.opretDagligFastOrdination(LocalDate.of(2017, 05, 05), LocalDate.of(2017, 05, 07), p1, lm, 50, 50,
				50, 50);
		assertNotNull(df);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdiationFail() {
		df = service.opretDagligFastOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 02, 28), p1, lm, 50, 50,
				50, 50);
		assertNotNull(df);
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		LocalTime[] bla = { LocalTime.of(8, 00), LocalTime.of(10, 00) };
		double[] antalEnheder = { 50, 50, 50 };

		ds = service.opretDagligSkaevOrdination(LocalDate.of(2017, 01, 01), LocalDate.of(2017, 01, 02), p1, lm, bla,
				antalEnheder);
		assertNotNull(ds);

		ds = service.opretDagligSkaevOrdination(LocalDate.of(2017, 8, 05), LocalDate.of(2017, 8, 10), p1, lm, bla,
				antalEnheder);
		assertNotNull(ds);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationFail() {
		LocalTime[] bla = { LocalTime.of(8, 00), LocalTime.of(10, 00) };
		double[] antalEnheder = { 50, 50, 50 };
		ds = service.opretDagligSkaevOrdination(LocalDate.of(2017, 03, 01), LocalDate.of(2017, 02, 28), p1, lm, bla,
				antalEnheder);
		assertNotNull(ds);
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		p1 = new Patient("00000000-.000", "Dennis", 100);
		lm = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(150, service.anbefaletDosisPrDoegn(p1, lm), 0.01);

		p1 = new Patient("00000000-.000", "Dennis", 24);
		lm = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(24, service.anbefaletDosisPrDoegn(p1, lm), 0.01);

		p1 = new Patient("00000000-.000", "Dennis", 126);
		lm = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(252, service.anbefaletDosisPrDoegn(p1, lm), 0.01);
	}

	@Test
	public void testAntalOrdinationerPrVaegtPrLaegemiddel() {

		assertEquals(0, service.antalOrdinationerPrVaegtPrLaegemiddel(1, 20, lm));

		assertEquals(0, service.antalOrdinationerPrVaegtPrLaegemiddel(20, 60, lm));

		assertEquals(0, service.antalOrdinationerPrVaegtPrLaegemiddel(100, 200, lm));
	}

}
